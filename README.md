# Figure-and-Figure-Caption-on-HTML5
Figure and Figure Caption on HTML5
<figure>
  <img src="images/otters.jpg" alt="Photograph of 
       two sea otters floating in water"   />
  <br />
  <figcaption>Sea otters holds hands when they
  sleep so they don't drift away from each
  other.</figcaption>
</figure>
